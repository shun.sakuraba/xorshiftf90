# README #

xorshift1024star.f90 is a Fortran 95 implementation of Sebastiano Vigna's xorshift1024*[1] pseudorandom generator. This random number generator fits almost all uses.

xorshift128plus.f90 is a Fortran 95 implementation of Sebastiano Vigna's xorshift128+[2] pseudorandom generator. This random generator is the best choice if you need an extra speed; otherwise, use xorshift1024\* rng. Note that xorshift1024\* is generally fast enough for most purpose (even compared to SIMD-friendly MT19937).

Usage:
```
#!Fortran

    ! Typical usage
    use xorshift1024star     ! open module at the beginning of subroutine or function
    ! use xorshift128plus    ! If you are a speed junkie

    ...
    
    call rand_init_self()    ! initialize global random number state with current time.
    ! call rand_init(seed)   ! ... or with a seed number.
    x = rand_uniform()       ! Draw a [0, 1) random number.
    i = rand_integer(42)     ! Draw an integer from [0, 42).
```

There are also functions which take state variable as an argument.

[1] Sebastiano Vigna. An experimental exploration of Marsaglia's xorshift generators, scrambled. CoRR, abs/1402.6246, 2014.
[2] Sebastiano Vigna. Further scramblings of Marsaglia's xorshift generators. CoRR, abs/1402.6246, 2014.
